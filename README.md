# Fortran comment alignment tool

Simply realign comments in Fortran code. I wrote it to fix the trailing
comments in our large Fortran codebase after beautifying it with
[fprettify](https://github.com/pseewald/fprettify).
