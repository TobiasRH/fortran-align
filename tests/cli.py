"""Command line interface (CLI) unit tests for falign."""
# noqa: E501
import unittest
import tempfile
import shutil
import subprocess
import sys
import os
# from typing import Tuple


def handle_rmtree_error(func, path, exc_info):
    """Error hanlde for rmtree."""
    print(f'{func} exception {exc_info}', file=sys.stderr)
    print(f'{func} could not delete {path}', file=sys.stderr)


class TestFAlignCLI(unittest.TestCase):
    """Unit tests for the command line interface (CLI) of falign."""

    def setUp(self):
        self.temp_dir: str = tempfile.mkdtemp(suffix='.tmp',
                                              prefix='TestFAlignCLI')
        self.falign_dir: str = os.path.abspath(
            os.path.join(
                os.path.dirname(
                    os.path.abspath(__file__)), '..'))

    def tearDown(self):
        shutil.rmtree(self.temp_dir, onerror=handle_rmtree_error)

    def helper_test_file_to_file(self, input_file: str, output_file: str,
                                 expected_file: str):
        """Helper for tests to test file to file."""

        input_data: str = ''
        output_data: str = ''
        expected_data: str = ''

        with open(input_file, 'r', encoding='ascii',
                  errors='surrogateescape') as file:
            input_data = file.read()
            file.close()

        with open(expected_file, 'r', encoding='ascii',
                  errors='surrogateescape') as file:
            expected_data = file.read()
            file.close()

        comp_proc = subprocess.run([sys.executable, '-m', 'falign',
                                    '--output', output_file,
                                    input_file],
                                   cwd=self.falign_dir,
                                   capture_output=True)

        self.assertEqual(comp_proc.returncode, 0,
                         '\n'.join([
                             '\n',
                             '- Command Output (stdout): --------------------',
                             comp_proc.stdout.decode(),
                             '- Command Output (stderr): --------------------',
                             comp_proc.stderr.decode(),
                             '-----------------------------------------------',
                         ]))

        with open(output_file, 'r', encoding='ascii',
                  errors='surrogateescape') as file:
            output_data = file.read()
            file.close()

        self.assertEqual(output_data, expected_data,
                         '\n'.join([
                             '\n',
                             '- Command Output (stdout): --------------------',
                             comp_proc.stdout.decode(),
                             '- Command Output (stderr): --------------------',
                             comp_proc.stderr.decode(),
                             '- Input: --------------------------------------',
                             input_data,
                             '- Output: -------------------------------------',
                             output_data,
                             '- Expected: -----------------------------------',
                             expected_data,
                             '-----------------------------------------------',
                         ]))

    def test_wrong_argument_inplace(self):
        """Test if wrong arguments to --inplace are detected correctly."""
        comp_proc = subprocess.run([sys.executable, '-m', 'falign',
                                    '--inplace'],
                                   cwd=self.falign_dir,
                                   capture_output=True,
                                   input='! test'.encode())
        self.assertEqual(comp_proc.returncode, 1,
                         '--inplace with stdin should return 1.')

        comp_proc = subprocess.run([sys.executable, '-m', 'falign',
                                    '--inplace',
                                    '--output',
                                    os.path.join(self.temp_dir,
                                                 'test_wrong_argument_inplace.f90')],
                                   cwd=self.falign_dir,
                                   capture_output=True,
                                   input='! test'.encode())
        self.assertEqual(comp_proc.returncode, 1,
                         '--inplace with --output should return 1.')

    def test_stdin_to_stdout(self):
        """Run falign with input from stdin and output to stdout."""
        comp_proc = subprocess.run([sys.executable, '-m', 'falign'],
                                   cwd=self.falign_dir,
                                   capture_output=True,
                                   input=' ! test'.encode())
        self.assertEqual(comp_proc.returncode, 0)
        self.assertEqual(comp_proc.stdout,
                         '                                                ! test'.encode())

    def test_file_to_stdout(self):
        """Run falign with input from stdin and output to stdout."""

        input_data: str = ''
        expected_data: str = ''

        # get input
        with open(os.path.join(self.falign_dir, 'data', 'test',
                               'basic_input.f90'), 'r') as file:
            input_data = file.read()
            file.close()

        # get expected result
        with open(os.path.join(self.falign_dir, 'data', 'test',
                               'basic_expected.f90'), 'r') as file:
            expected_data = file.read()
            file.close()

        comp_proc = subprocess.run([sys.executable, '-m', 'falign',
                                    os.path.join(self.falign_dir, 'data', 'test', 'basic_input.f90'), ],
                                   cwd=self.falign_dir,
                                   capture_output=True)
        self.assertEqual(comp_proc.returncode, 0)
        self.assertEqual(comp_proc.stdout.decode(), expected_data,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             input_data,
                             '- Output: -------------------------------------',
                             comp_proc.stdout.decode(),
                             '- Expected: -----------------------------------',
                             expected_data,
                             '-----------------------------------------------',
                         ]))

    def test_file_to_file(self):
        """Run falign with input_file from file and output to file."""

        self.helper_test_file_to_file(
            os.path.join(self.falign_dir, 'data', 'test', 'basic_input.f90'),
            os.path.join(self.temp_dir, 'basic_output.f90'),
            os.path.join(self.falign_dir, 'data', 'test',
                         'basic_expected.f90'))

    def test_encoding_iso8859_15(self):
        """Test how none UTF-8 encodings work with iso8859-15."""

        self.helper_test_file_to_file(
            os.path.join(self.falign_dir, 'data', 'test',
                         'encoding_iso8859-15_input.f90'),
            os.path.join(self.temp_dir, 'encoding_iso8859-15_output.f90'),
            os.path.join(self.falign_dir, 'data', 'test',
                         'encoding_iso8859-15_expected.f90'))

    def test_encoding_utf_8(self):
        """Test how UTF-8 encodings work."""

        self.helper_test_file_to_file(
            os.path.join(self.falign_dir, 'data', 'test',
                         'encoding_utf-8_input.f90'),
            os.path.join(self.temp_dir, 'encoding_iso8859-15_output.f90'),
            os.path.join(self.falign_dir, 'data', 'test',
                         'encoding_utf-8_expected.f90'))

    def test_inplace(self):
        """Run falign with input from stdin and output to stdout."""

        input_file: str = ''
        expected: str = ''
        output_file: str = os.path.join(self.temp_dir, 'test_file_to_file.f90')

        # get input
        with open(os.path.join(self.falign_dir, 'data', 'test',
                               'basic_input.f90'), 'r') as file:
            input_file = file.read()
            file.close()

        # get expected result
        with open(os.path.join(self.falign_dir, 'data', 'test',
                               'basic_expected.f90'), 'r') as file:
            expected = file.read()
            file.close()

        # write in an output file
        with open(output_file, 'w') as file:
            file.write(input_file)
            file.close()

        comp_proc = subprocess.run([sys.executable, '-m', 'falign',
                                    '--inplace', output_file, ],
                                   cwd=self.falign_dir,
                                   capture_output=True)
        self.assertEqual(comp_proc.returncode, 0)

        # get actual result
        with open(output_file, 'r') as file:
            output = file.read()
            file.close()

        self.assertEqual(output, expected,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             input_file,
                             '- Output: -------------------------------------',
                             output,
                             '- Expected: -----------------------------------',
                             expected,
                             '-----------------------------------------------',
                         ]))

if __name__ == '__main__':
    unittest.main()
