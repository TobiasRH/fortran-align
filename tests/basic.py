"""Basic unit tests for falign."""
import unittest
from typing import List
from falign.falign import FAlign


class TestFAlignBasic(unittest.TestCase):
    """Unit tests for module falign."""

    def test_align_nothing_to_do(self) -> None:
        """Test with code with nothing to falign."""

        falign = FAlign()

        input_value = [
            'subroutine test_subroutine(int)',
            '  integer :: int',
            '  if (int .eq. 1) then',
            '  end if',
            'end subroutine test_subroutine',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, input_value)

    def test_falign(self) -> None:
        """Test with code with comments to falign."""

        falign = FAlign()

        input_value = [
            'subroutine test_subroutine(int) ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then ! Line 3',
            '  end if ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"     ! Line 6',
            '  char = \'Noch ein String mit Trennern! "" :)\'  ! Line 7',
            'end subroutine test_subroutine   ! Line 8',
        ]
        expected_value = [
            'subroutine test_subroutine(int) ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then       ! Line 3',
            '  end if                        ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"               ! Line 6',
            '  char = \'Noch ein String mit Trennern! "" :)\' ! Line 7',
            'end subroutine test_subroutine                 ! Line 8',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_ignore_offset(self) -> None:
        """Test."""

        falign = FAlign(2, 10, 2)

        input_value = [
            'subroutine test',
            '  character(20) :: c',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '   ! Comment to align with following end if',
            '  end if',
            '',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '    ! Comment to align with following end if',
            '  end if',
            '',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '     ! Comment to align in column 10',
            '  end if',
            'end subroutine test',
        ]
        expected_value = [
            'subroutine test',
            '  character(20) :: c',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '  ! Comment to align with following end if',
            '  end if',
            '',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '  ! Comment to align with following end if',
            '  end if',
            '',
            '  if (.true. == .true.) then',
            '    if (.true. == .true.)',
            '    end if',
            '         ! Comment to align in column 10',
            '  end if',
            'end subroutine test',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_post_code_comment(self) -> None:
        """Test comments behind the code."""

        falign = FAlign(2, 10, 2)

        input_value = [
            'module test_m',
            'contains',
            '  subroutine test',
            '    character(20) :: c ! Behind declaration',
            '    ! comment following post code comment',
            ' ',
            '    character(20) :: c ! Behind declaration',
            '    ! comment following post code comment',
            '    ! comment following commoent following post code comment',
            ' ',
            '    character(20) :: c',
            '    ! comment following code',
            ' ',
            '  end subroutine test',
            'end module test_m',
        ]
        expected_value = [
            'module test_m',
            'contains',
            '  subroutine test',
            '    character(20) :: c  ! Behind declaration',
            '                        ! comment following post code comment',
            ' ',
            '    character(20) :: c  ! Behind declaration',
            '                        ! comment following post code comment',
            '                        ! comment following commoent following post code comment',
            ' ',
            '    character(20) :: c',
            '  ! comment following code',
            ' ',
            '  end subroutine test',
            'end module test_m',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_align_min_space_3_start_column_41(self) -> None:
        """Test with code with comments to falign with minimum 3 space and
        start column 41."""

        falign = FAlign(min_space_before_comment=3, comment_start_column=41)

        input_value = [
            'subroutine test_subroutine(int) ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then ! Line 3',
            '  end if ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"     ! Line 6',
            '  char = \'Noch ein .. String mit Trennern! "" :)\'  ! Line 7',
            'end subroutine test_subroutine   ! Line 8',
        ]
        expected_value = [
            'subroutine test_subroutine(int)         ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then               ! Line 3',
            '  end if                                ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"                    ! Line 6',
            '  char = \'Noch ein .. String mit Trennern! "" :)\'   ! Line 7',
            'end subroutine test_subroutine                      ! Line 8',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_align_long_lines(self) -> None:
        """Test if comments with to much indend after the code are."""
        """put to the back to the left."""

        falign = FAlign(min_space_before_comment=3, comment_start_column=10)

        input_value = [
            'write (*, *) &',
            '  "Hello World I`m a paceholder for a ", &           ! Kommentar',
            '  " real-world example Foo Bar and Baz."              ! Folge Zeile'
        ]
        expected_value = [
            'write (*, *) &',
            '  "Hello World I`m a paceholder for a ", &   ! Kommentar',
            '  " real-world example Foo Bar and Baz."     ! Folge Zeile'
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

        falign = FAlign(min_space_before_comment=1, comment_start_column=10)

        input_value = [
            'write (*, *) &',
            '  "Hello World I`m a paceholder for ä ", &           ! Kommentar',
            '  "german Umlaut äöüÄÖÜß€", & ! Noch ein Kommentar',
            '  " real-world example Foo Bar and Baz."        ! Folge Zeile'
        ]
        expected_value = [
            'write (*, *) &',
            '  "Hello World I`m a paceholder for ä ", & ! Kommentar',
            '  "german Umlaut äöüÄÖÜß€", &              ! Noch ein Kommentar',
            '  " real-world example Foo Bar and Baz."   ! Folge Zeile'
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_align_min_space_3_start_column_41_to_wide_indent(self) -> None:
        """Test with code with comments which are to wide indented to falign
        with minimum 3 space and start column 41."""

        falign = FAlign(min_space_before_comment=3, comment_start_column=41)

        input_value = [
            'subroutine test_subroutine(int)                        ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then              ! Line 3',
            '  end if ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"                       ! Line 6',
            '  char = \'Noch ein .. String mit Trennern! "" :)\'     ! Line 7',
            'end subroutine test_subroutine                     ! Line 8',
        ]
        expected_value = [
            'subroutine test_subroutine(int)         ! Line 1',
            '  character(10) :: char',
            '  if (char .eq. \'1\') then               ! Line 3',
            '  end if                                ! Line 4',
            ' ',
            '  char = "String \' mit Trenner!"                    ! Line 6',
            '  char = \'Noch ein .. String mit Trennern! "" :)\'   ! Line 7',
            'end subroutine test_subroutine                      ! Line 8',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_align_block_comment(self) -> None:
        """Test with code with comments to falign."""

        falign = FAlign()

        # pylint: disable=protected-access

        input_value = [
            'subroutine test_subroutine(int) ! Line 1',
            '  ! Line 2 comment with code indent',
            '  character(10) :: char',
            '    ! Line 3 comment withOUT code indent',
            '  if (char .eq. \'1\') then ! Line 5',
            ' ! Line 6 comment before code',
            '! Line 7 at comment at start of line',
            '  end if  ! Line 8',
        ]
        expected_value = [
            'subroutine test_subroutine(int) ! Line 1',
            '                                ! Line 2 comment with code indent',
            '  character(10) :: char',
            '                                ! Line 3 comment withOUT code indent',
            '  if (char .eq. \'1\') then       ! Line 5',
            '                                ! Line 6 comment before code',
            '! Line 7 at comment at start of line',
            '  end if                        ! Line 8',
        ]
        output_value = input_value

        falign._align_block_comments(output_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

        input_value = [
            '  char = "String \' mit Trenner!"  ! Line 6',
            '  char = \'Noch ein String mit Trennern! "" :)\' ! Line 7',
            'end subroutine test_subroutine ! Line 8',
        ]
        expected_value = [
            '  char = "String \' mit Trenner!"               ! Line 6',
            '  char = \'Noch ein String mit Trennern! "" :)\' ! Line 7',
            'end subroutine test_subroutine                 ! Line 8',
        ]
        output_value = input_value

        # pylint: disable=protected-access
        falign._align_block_comments(output_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_align_single_comment(self) -> None:
        """Test for a comments outside a code block."""

        falign = FAlign(1, 31)

        # pylint: disable=protected-access

        input_value = [
            'subroutine test_subroutine(int)',
            ' ',
            ' ! 1: Comment should be aligned with next code line.',
            ' ',
            '  character(10) :: char',
            ' ',
            '! 2: Comment should be left at first column.',
            '    ! 3: Comment should start in column 31.',
            ' ',
            ' ! 4: Comment should be aligned with following code line.',
            ' ',
            ' ! 5: Comment should be aligned with next code line.',
            ' ',
            '  if (char .eq. \'1\') then ! 6: Comment should start in column 31.',
            ' ',
            ' ! 7: Comment should be aligned with next code line.',
            ' ',
            '    write (*,*) "Hello World!"',
            '  end if',
            ' ',
            ' ! 8: Comment should start in column 31.',
            '! 9: Comment should be left at first column.',
            'end subroutine test_subroutine'
        ]
        expected_value = [
            'subroutine test_subroutine(int)',
            ' ',
            '  ! 1: Comment should be aligned with next code line.',
            ' ',
            '  character(10) :: char',
            ' ',
            '! 2: Comment should be left at first column.',
            '                              ! 3: Comment should start in column 31.',
            ' ',
            '  ! 4: Comment should be aligned with following code line.',
            ' ',
            '  ! 5: Comment should be aligned with next code line.',
            ' ',
            '  if (char .eq. \'1\') then     ! 6: Comment should start in column 31.',
            ' ',
            '    ! 7: Comment should be aligned with next code line.',
            ' ',
            '    write (*,*) "Hello World!"',
            '  end if',
            ' ',
            '                              ! 8: Comment should start in column 31.',
            '! 9: Comment should be left at first column.',
            'end subroutine test_subroutine'
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_trailing_lines(self) -> None:
        """Test for lost trailing lines."""

        falign = FAlign(1, 31)

        # pylint: disable=protected-access

        input_value = [
            'subroutine test_subroutine(int)',
            'end subroutine test_subroutine',
            '',
        ]
        expected_value = [
            'subroutine test_subroutine(int)',
            'end subroutine test_subroutine',
            '',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

        input_value = [
            'subroutine test_subroutine(int)',
            'end subroutine test_subroutine',
            ' ',
        ]
        expected_value = [
            'subroutine test_subroutine(int)',
            'end subroutine test_subroutine',
            ' ',
        ]

        output_value = falign.falign(input_value)

        self.assertEqual(output_value, expected_value,
                         '\n'.join([
                             '\n',
                             '- Input: --------------------------------------',
                             '\n'.join(input_value),
                             '- Output: -------------------------------------',
                             '\n'.join(output_value),
                             '- Expected: -----------------------------------',
                             '\n'.join(expected_value),
                         ]))

    def test_next_block_slice(self) -> None:
        """Test which looks for blocks in code."""

        falign = FAlign()

        input_value = [
            'Line 1',
            'Line 2',
            ' ',
            'Line 4',
            ' ',
            'Line 6',
        ]

        block = falign.next_block_slice(0, input_value)
        self.assertEqual(block, slice(0, 3, None))

        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(3, 5, None))

        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(5, 6, None))

        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(None, None, None))

    def test_next_block_slice_empty_block(self) -> None:
        """Test with no blocks, completly empty."""

        falign = FAlign()

        input_value = [
            '',
            '     \t',
            '\n',
            ' ',
            ' ',
        ]

        # Test if whitespaces are treated as empty lines
        block = falign.next_block_slice(0, input_value)
        self.assertEqual(block, slice(0, 5, None))

        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(None, None, None))

    def test_next_block_slice_empty_input_value(self) -> None:
        """Test completly empty."""

        falign = FAlign()

        input_value: List[str] = []

        # Test if empty list results in empty block
        block = falign.next_block_slice(0, input_value)
        self.assertEqual(block, slice(None, None, None))

    def test_next_block_slice_leading_and_trailing_empty_lines(self) -> None:
        """Test with just one block and leading and trailing empty lines."""

        falign = FAlign()

        input_value = [
            '',
            '',
            ' ',
            'Line 4',
            ' ',
            '',
        ]

        # Find block with first three empty lines
        block = falign.next_block_slice(None, input_value)
        self.assertEqual(block, slice(0, 3, None))

        # Find block with line four and the two trailing empty lines
        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(3, 6, None))

        # Last block is empty
        block = falign.next_block_slice(block.stop, input_value)
        self.assertEqual(block, slice(None, None, None))

    def test_comment_index(self) -> None:
        """Test for the comment finder."""

        falign = FAlign()

        # pylint: disable=protected-access
        self.assertEqual(falign._comment_index('!'), 0)
        self.assertEqual(falign._comment_index(' ! Text'), 1)
        self.assertEqual(falign._comment_index('  ! Text!'), 2)
        self.assertEqual(falign._comment_index(' ! Text '), 1)
        self.assertEqual(falign._comment_index('  ! Text! '), 2)
        self.assertEqual(falign._comment_index(' "String" !'), 10)
        self.assertEqual(falign._comment_index(' \'String\' !'), 10)
        self.assertEqual(falign._comment_index(' "String\'" !'), 11)
        self.assertEqual(falign._comment_index(' \'String"\' !'), 11)
        self.assertEqual(falign._comment_index(' "String!" !'), 11)
        self.assertEqual(falign._comment_index(' \'String!\' !'), 11)
        self.assertEqual(falign._comment_index(' "String!\'" !'), 12)
        self.assertEqual(falign._comment_index(' \'String!"\' !'), 12)
        self.assertEqual(falign._comment_index(' ""!'), 3)
        self.assertEqual(falign._comment_index(' \'\'!'), 3)
        self.assertEqual(falign._comment_index(''), None)
        self.assertEqual(falign._comment_index(' "String!"'), None)
        self.assertEqual(falign._comment_index(' \'String!\''), None)
        self.assertEqual(falign._comment_index(' "String!\'" Bla'), None)
        self.assertEqual(falign._comment_index(' \'String!"\' Bla'), None)

    def test_first_none_whitespace_index(self) -> None:
        """Test for the first non whitespace finder."""

        falign = FAlign()

        # pylint: disable=protected-access
        self.assertEqual(falign._first_non_whitespace_index('!'), 0)
        self.assertEqual(falign._first_non_whitespace_index(' a'), 1)
        self.assertEqual(falign._first_non_whitespace_index('\t!'), 1)
        self.assertEqual(falign._first_non_whitespace_index('\n!'), 1)
        self.assertEqual(falign._first_non_whitespace_index(''), None)
        self.assertEqual(falign._first_non_whitespace_index('\n \t'), None)

    def test_first_none_whitespace(self) -> None:
        """Test for the first non whitespace finder."""

        falign = FAlign()

        # pylint: disable=protected-access
        self.assertEqual(falign._first_non_whitespace('!'), '!')
        self.assertEqual(falign._first_non_whitespace(' !'), '!')
        self.assertEqual(falign._first_non_whitespace('  !'), '!')
        self.assertEqual(falign._first_non_whitespace('! Bla'), '!')
        self.assertEqual(falign._first_non_whitespace(' ! Bla'), '!')
        self.assertEqual(falign._first_non_whitespace('  ! Bla'), '!')
        self.assertEqual(falign._first_non_whitespace(' a'), 'a')
        self.assertEqual(falign._first_non_whitespace('\t!'), '!')
        self.assertEqual(falign._first_non_whitespace('\n!'), '!')
        self.assertEqual(falign._first_non_whitespace(''), None)
        self.assertEqual(falign._first_non_whitespace('\n \t'), None)

    def test_code_indent(self) -> None:
        """Test for finding the actual code indent."""

        falign = FAlign()

        input_value = [
            '! Comment before routine',
            '! Second line in comment',
            'subroutine test () ! Comment',
            '  integer :: i',
            ' ',
            '  ! Comment before block',
            '  if (i == 0) then',
            '  ! Comment in block',
            '    write(*,*) "Hello World!"',
            '  ! Comment before endif',
            '  endif',
            '  ! Comment before end of subroutine',
            'end subroutine',
            '! Comment at end of subroutine'
        ]

        # pylint: disable=protected-access
        self.assertEqual(
            falign._code_indent(input_value[0:]), 0, input_value[0:])
        self.assertEqual(
            falign._code_indent(input_value[1:]), 0, input_value[1:])
        self.assertEqual(
            falign._code_indent(input_value[2:]), 0, input_value[2:])
        self.assertEqual(
            falign._code_indent(input_value[3:]), 2, input_value[3:])
        self.assertEqual(
            falign._code_indent(input_value[4:]), 2, input_value[4:])
        self.assertEqual(
            falign._code_indent(input_value[5:]), 2, input_value[5:])
        self.assertEqual(
            falign._code_indent(input_value[6:]), 2, input_value[6:])
        self.assertEqual(
            falign._code_indent(input_value[7:]), 4, input_value[7:])
        self.assertEqual(
            falign._code_indent(input_value[8:]), 4, input_value[8:])
        self.assertEqual(
            falign._code_indent(input_value[9:]), 2, input_value[9:])
        self.assertEqual(
            falign._code_indent(input_value[10:]), 2, input_value[10:])
        self.assertEqual(
            falign._code_indent(input_value[11:]), 0, input_value[11:])
        self.assertEqual(
            falign._code_indent(input_value[12:]), 0, input_value[12:])
        self.assertEqual(
            falign._code_indent(input_value[13:]), None, input_value[13:])


if __name__ == '__main__':
    unittest.main()
