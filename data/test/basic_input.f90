program test
  integer :: i ! Test Comment indent
  real :: r ! Comment

 ! Boom - should be code indent
  if (i - r > 5) then ! Next Line
 ! Will be indented with the comment in the line before :D
    write (*,*) 'Hello World!'
  end if

 ! Should be code indent
   write (*,*) 'Foo Bar Barz :)'

        ! Lonely Argument without Block behind current Code indent -> Line 49
 ! Lonely Argument without Block befor Code indent -> Line 1

 ! Shoult be code indent too
end program test

